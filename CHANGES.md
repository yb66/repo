# Changes #

## 0.11.7 - Tuesday the 30th of January 2018 ##

- Fixed a bug with console, should use Object.const_set not Module.
  You live and you learn.
- Made pry a formal requirement.

----


## 0.11.6, 0.11.5 - Wednesday the 6th of December 2017 ##

* Running `repo db migrate --to=` would fail because the code
  was written for a Rake task, which is no longer the case.

----


## 0.11.4 - Saturday the 14th of October 2017 ##

* Can pass the config(s) location to the Environment init
  if desired. It's convenient.

----
 

## 0.11.3, 0.11.2, 0.11.1, 0.11.0 - Friday the 13th of October 2017 ##

* Don't reset the logging level inadvertently.

----


* Bugfix, its Logging not Logger :-/

----


* A default logger for the Environment class too.

-----


* Nested exceptions for the ModelDirs class.
* A default logger, mainly for when using the db class
  without the cmdline.

----


## 0.10.0 - Sunday the 7th of October 2017 ##

* Improved the constring munging so that .test can easily
  be added.

----


## 0.9.1, 0.9.0 - Friday the 29th of September 2017 ##

* Running on installation won't be via exe/ so it fails the 
  conditional test and exits early. Fixed.

----

* Structural changes to the code. It was becoming hard
  to fix certain things and bolt on new commands. Needed a
  proper clean up.
* Updated spec for environment.
* Console works.
* Catches some errors and displays debug automatically.
* Env passes around better.
* Connstring generation more sound, can be overridden,
  doesn't ignore options.
* Added DB_ROLE env var.

----


## 0.8.0 - Monday the 25th of September 2017 ##

* OpenSSL implementation has changed to throw errors so
  keeping the secret key length to only 32 bytes.

----


## 0.7.3, 0.7.4 - Friday the 22nd of September 2017 ##

* The db_exists? test wasn't working as expected, hopefully
  this one works better.
* It wasn't working better as I forgot $? in Ruby is an object
  and not a number as it is in the shell.

----


## 0.7.2 ##

* Fixed a bug in Repo.require_models.
* Updated its API slightly to allow re-requiring.
  No need for semver jump though.

----



## 0.7.1 - Thursday the 21st of September 2017 ##

* Was using the wrong key with --set_connstring which meant the
  switch wouldn't fire. Fixed.

----


## 0.7.0 ##

* Restructured the files to make the db class more manageable.
* Fixed more options, such as the backup/restore.
* Slight change to some options to make clearer/fix a few
   incorrect or vage options.
* Ever so little more documentation. It's a start.

----


## 0.6.3 - Wednesday the 20th of September 2017 ##

* Using double quotes to protect against dreaded syntax errors
  for when role names contain a hyphen, as my test user did.

----


## 0.6.2 ##

* Granting privileges takes several statements, was missing one
  for tables that already exist. Since granting is not just done
  at creation an extra command seemed helpful.

----


## 0.6.1 ##

* There were load errors when installing the gem because Gem.bin_path
  uses "bin/" as its default. Didn't know this, now I do! Fixed.

----


## 0.6.0 - Tuesday the 19th of September 2017 ##

* Using scripts now instead of rake.
* Using Docopt to parse options, it's allowed for an easier
  way to run the commands than rake (after much rejigging).
* Better output to help the consumer.

----


## 0.5.0 - Saturday the 9th of September 2017 ##

* Fixed an intermittent bug.
* Added the db/backup dir to the setup.

----


## v0.4.0 - Friday the 8th of September 2017 ##

* Can also pass configs via arguments.

----


## v0.3.0 - Friday, 8th of September 2017 ##

* Can pass configs that are in non-standard locations.

----


## v0.2.0 - Wednesday, 30th of March 2016 ##

* Can set up standard config files et al via templates.

## v0.1.1 ##

* Bug fixes.

## v0.1.0 ##

* Moved the environment stuff in.

## v0.0.3 - Wed, 8 Aug 2012 ##

* Added a bit of error checking for the logger, and tightened up the arguments. Bit of documentation too.

## v0.0.2 - Wed, 8 Aug 2012 ##

* Fixed some specs and minor problems.

## v0.0.1 - 17th of July 2012 ##

* First add.

----
