# (Sequel) Repo

## Why? ##

Two reasons. Firstly, to save me writing the same old bootstrapping code for Sequel every time and possibly getting some wrong, I just use this.

Secondly, I don't like using the constant `DB`. I prefer a class instance variable, it allows slightly more protection and a bit more leeway to hook things in (at some point).

## Installation

I haven't released this as a gem yet, but I probably will soon. For now, you can either download it or link to this Git repository, it will stay public.

## Usage

Example:

    # I tend to put all this stuff 
    # in the config.ru if it's a web app.

    logger = Logger.new STDOUT
    logger.level = Logger::WARN
    logger.datetime_format = '%a %d-%m-%Y %H%M '
    set :logger, logger
    
    Repo::Config.logger = logger
    Repo::Config.connstring = ENV['DATABASE_URL']
    Repo::Config.extensions = [:pg_hstore,:pagination]
    Repo.model_dirs << "./app/models"
    Repo.db # make sure it's initialised and working.
    Repo.require_models # you don't need to do this, but I like to.

From then on the database is available with `Repo.db`, anywhere in your code:

    Repo.db.filter( :your => "stuff here" ).all

## Contributing

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Added some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request
