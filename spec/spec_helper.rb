# encoding: UTF-8

require 'rspec'
require 'rspec/its'
require 'rspec/given'
require 'pathname'

if ENV["DEBUG"]
  require 'pry-byebug'
  require 'pry-state'
  binding.pry
end


Spec_dir = Pathname(__dir__)
Fixtures_dir = Spec_dir.join("support/fixtures")

require "logger"
logger = Logger.new STDOUT
logger.level = Logger::DEBUG
logger.datetime_format = '%a %d-%m-%Y %H%M '
LOgger = logger


# freeze time!
# require 'timecop'
# Timecop.freeze( Time.local( 2011, 10, 2, 12, 0, 0 )  )
RSpec.configure do |config|
  # Restore the state of ENV after each spec
  config.before { @env_keys = ENV.keys }
  config.after  { ENV.delete_if { |k, _v| !@env_keys.include?(k) } }
end