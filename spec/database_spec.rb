require 'spec_helper'

require_relative "../lib/repo/db.rb"

describe Repo::Database do
  def env
    Repo::Logging.level = :quiet
    logger = Repo::Logging.instance
    @env ||= Repo::Environment.new
    @env.logger = logger
    @env.configs = Spec_dir
    @env
  end

  context "Straightforward" do
    Given(:db) { Repo::Database.new env, logger: env.logger }

    When(:connstring) { db.connstring }
    Then { connstring == "postgres://example@localhost/example" }
  end

  context "Using the test db" do
    context "via RACK_ENV" do
      Given(:db) { Repo::Database.new env, logger: env.logger }
      before do
        @rack_env_before = ENV["RACK_ENV"]
        ENV["RACK_ENV"] = "test"
      end
      after do
        ENV["RACK_ENV"] = @rack_env_before
      end

      When(:connstring) { db.connstring }
      Then { connstring == "postgres://example@localhost/example.test" }
    end
    context "via an argument" do
      Given(:db) { Repo::Database.new env, logger: env.logger, test: true }

      When(:connstring) { db.connstring }
      Then { connstring == "postgres://example@localhost/example.test" }
    end
  end
end