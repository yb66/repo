Sequel.migration do

  # This migration is for…
  up do
    warn "001 ->"

      ct = Sequel::CURRENT_TIMESTAMP

      create_table :tasks do
        primary_key :id, type: :Bignum, null: false
        String :content, null: false
        DateTime :created_at, null: false, default: ct
        DateTime :updated_at, null: false, default: ct
      end
    warn "001 done!"
  end # up
  
  down do
    warn "001 ->"
      drop_table :tasks
    warn "001 done!"  
  end # down
end # migration