# only load this when db is needed

#get the app
require_relative "../app/config.rb"

Fixtures = File.expand_path( File.join Spec_dir, "/fixtures/" ) 


require "sequel"
require_relative File.expand_path( File.join Spec_dir, "../lib/repo.rb" )

# dev_db_path = File.expand_path( File.join Spec_dir, "../app.db" )
# test_db_path = File.expand_path( File.join Spec_dir, "../test.db" )
# File.delete test_db_path if File.exists? test_db_path 
# DB = Sequel.connect("sqlite://#{test_db_path}")

# Sequel.extension :migration
# # Migrate the DB to The Present
# Sequel::Migrator.run(DB, File.expand_path( File.join Spec_dir, "../db/migrations" ) ) 

# logging for the db
Repo::Config.logger = logger
Repo::Config.connstring = ENV['DATABASE_URL']
DB = Repo.db

# get the fixtures
require 'ffaker'
require 'fabrication'

Midas::Role.find_or_create(role: "test")