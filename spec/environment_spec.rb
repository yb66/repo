# encoding: UTF-8

require 'spec_helper'
require_relative "../lib/repo/environment.rb"
require_relative "../lib/repo/environment/plugins.rb"
load Pathname(__dir__).join( "../exe/repo_env" )

def env
  @env ||= Repo::Environment.new
  @env.configs = File.join(__dir__, "support/fixtures")
  @env
end

def env_with_opts **opts
  @env ||= Repo::Environment.new opts
  @env
end

describe Repo::Environment do
  describe :initialize do
    context "No opts" do
      When(:e) { env }
      Then { e.config_files.keys == [Fixtures_dir.join("config/initializers/sequel.yml")] }
    end
    context "Given opts" do
      When(:e) { env_with_opts configs: Fixtures_dir.join("config") }
      Then { e.config_files.keys == [Fixtures_dir.join("config/initializers/sequel.yml")] }
    end
  end
  describe :load_file do
    Given(:logger) {
      Repo::Logging.level = :quiet
      logger = Repo::Logging.instance
    }
    context "Given a YAML file" do
      context "With no futher args" do
        context "Sequel" do
          before do
            env.load :sequel
          end
          after :all do
            env = nil
          end
          subject{ ENV }
          its(["DB_APP"]) { should == "example" }
          its(:keys) { should include "DB_APP" }
          its(:keys) { should include "SEQUEL_EXTENSIONS" }
          its(:keys) { should include "SEQUEL_PLUGINS" }
          its(["SEQUEL_EXTENSIONS"]) { should == "pg_json,pg_array" }
          its(["SEQUEL_PLUGINS"]) { should == "many_through_many,tree" }
        end
      end
    end
    describe :set_connstring do
      context "Given no args" do
        Given(:expected) { "postgres://example@localhost/example" }
        before do
          env.load :sequel
        end
        after :all do
          env = nil
        end
        When(:conn) { env.set_connstring }
        Then { conn == expected }
        And { ENV["DATABASE_URL"] == expected }
      end
      context "Given a role" do
        Given(:role) { "exemplar" }
        Given(:expected) { "postgres://#{role}@localhost/example" }
        before do
          env.role = role
        end
        after :all do
          env = nil
        end
        When(:conn) { env.set_connstring refresh: true }
        Then { env.role == role }
        Then { env.name == ENV["DB_APP"] }
        Then { conn == expected }
        Then { ENV["DATABASE_URL"] == expected }
      end
      context "set to test" do
        Given(:expected) { "postgres://example@localhost/example.test" }
        before do
          env.load :sequel
        end
        after :all do
          env = nil
        end
        When(:conn) { env.set_connstring test: true }
        Then { conn == expected }
        And { ENV["DATABASE_URL"] == expected }
      end
    end
  end
end