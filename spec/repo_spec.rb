# encoding: UTF-8

require 'spec_helper'
require_relative "../lib/repo.rb"

describe "ModelDirs" do
  
  context "Instantiation" do
  
    context "Given no args" do
      subject { Repo::Config::ModelDirs.new }
      it { should be_a_kind_of Array }
      it { should respond_to :push }
      it { should respond_to :<< }
    end
  end
  
  describe "push" do
    Given(:m) { Repo::Config::ModelDirs.new }
    context "Given no args" do
      it "should raise a ArgumentError" do
        expect { m.push }.to raise_error ArgumentError
      end
    end
    
    context "Given a string" do
      context "That is empty" do
        Then {
          expect { m.push "" }.to raise_error Repo::Config::ModelDirs::Error
        }
      end
      context "That is not empty" do
        Given(:current_dir) { "." }
        When(:dirs) { m.push current_dir }
        Then { dirs.kind_of? Array }
        Then { dirs.include? Pathname(File.expand_path(current_dir)) }
      end
    end
    
    context "Given a directory object (Dir)" do
      Given(:current_dir) { Dir.new "." }
      When(:dirs) { m.push current_dir }
      Then { dirs.kind_of? Array }
      Then { dirs.include? Pathname(File.expand_path(current_dir)) }
    end
    
    context "Given an array" do
      context "That is empty" do
        it "should raise an error" do
          expect { m.push [] }.to raise_error Repo::Config::ModelDirs::Error
        end
      end
      context "That is not empty" do
        it "should raise an error" do
          expect { m.push [Dir.new(".")] }.to raise_error Repo::Config::ModelDirs::Error
        end
      end
    end
    
  end


end