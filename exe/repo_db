require_relative "../lib/repo/cmd.rb"
require_relative "../lib/repo/db.rb"

module Repo
  module Cmd
    class Db < Base

      USAGE= <<USAGE
Usage:
  repo db version [--role=<rolename>] [--name=<dbname>]
  repo db connstring [--role=<rolename>] [--name=<dbname>] [--test]
  repo db setup [--test]
  repo db exist (--role=<rolename> | --name=<dbname>) 
  repo db create [--role=<rolename>] [--name=<dbname>] [--grant]
  repo db grant [--role=<rolename>] [--name=<dbname>]
  repo db drop --name=<dbname> [--role=<rolename>]
  repo db migrate ( --up | --down | --to=<migration> | --reset) [--path=<path>] [--role=<rolename>] [--name=<dbname>]
  repo db migrate --trash [--role=<rolename>] [--name=<dbname>]
  repo db restore [--path=<path>] [--role=<rolename>] [--name=<dbname>] [--custom]
  repo db (backup | dump) [--path=<path>] [--role=<rolename>] [--name=<dbname>] [--custom]

Options:
  --role=<rolename>       Database role name, defaults to the database name
  --name=<dbname>         Database name.
  --grant                 Run the commands to grant privileges.
  --up                    Migrate up.
  --down                  Migrate down.
  --to=<migration>        Migrate to a given migration number.
  --reset                 Go all the way down then all the way up.
  --trash                 Drop all tables.
  --path=<migrations>     When used with migrate it is the path to the migrations. When used with restore/backup it is where to restore from or backup to.
  --custom                Use to get a custom backup/restore. See postgres docs.
  --test                  Append '.test' to database name.
USAGE
      USAGE.freeze


      def execute
        logger.vverbose "Executing db"
        logger.debug("Options passed to db:") { options }

        opts = {
          logger: logger,
          path: options["--path"],
          role: options["--role"],
          name: options["--name"],
          test: options["--test"]
        }.reject{|k,v| v.nil?}

        db = ::Repo::Database.new env, opts

        if options["version"]
          warn db.version
        elsif options["setup"]
          db.setup
        elsif options["connstring"]
          warn db.connstring # no need to check quiet, just ignore it
        elsif options["exist"]
          if options ["--name"]
            warn db.db_exists?
          end
          if options ["--role"]
            warn db.role_exists? options ["--role"]
          end
        elsif options["create"]
            db.create_db
          if options["--role"]
            db.create_role
          end
          if options["--name"]
            db.create_db
          end
          if options["--grant"]
            warn "When using 'create --grant' you must supply either a --role or a --name or both" and exit unless options ["--name"] or options["--role"]     
            db.grant_role
          end
        elsif options["migrate"]
          if options["--up"]
            db.up
          elsif options["--down"]
            db.down
          elsif options["--to"]
            db.to options["--to"]
          elsif options["--reset"]
            db.reset
          elsif options["--trash"]
            db.trash
          end
        elsif options["grant"]
          db.grant_role
        elsif options["restore"]
          db.restore options["--path"], custom: options["--custom"]
        elsif options["backup"]
          db.backup options["--path"], custom: options["--custom"]
        end
      rescue Sequel::DatabaseConnectionError => e
        warn e.message
        logger.output_unless_quiet("Options passed to db:") { options }
        logger.output_unless_quiet "Connection string: #{@url}"
        raise e
      end
    end
  end
end

Repo::Cmd::Db.run if __FILE__ == $0