require_relative "../lib/repo/cmd.rb"

module Repo
  module Cmd
    class Env < Repo::Cmd::Base
      USAGE= <<USAGE
Usage:
  repo env  [--configs <locations>...] [--set_connstring] [--test]

Options:
  --configs <locations>...  Location of config files/dirs.
  --role=<rolename>         Database role name, defaults to the database name.
  --name=<dbname>           Database name.
  --test                    Append '.test' to database name.
USAGE
      USAGE.freeze


      def execute
        env.role = options["--role"] if options["--role"]
        env.name = options["--name"] if options["--name"]
        if env.dry_run?
          env.logger.output_unless_quiet "Environment still set up in dry-run mode."
        end
        env.load_all
        env.set_connstring if options["--set_connstring"]
      end
      
    end
  end 
end


Repo::Cmd::Env.run if __FILE__ == $0