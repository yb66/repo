# -*- encoding: utf-8 -*-
require File.expand_path('../lib/repo/version', __FILE__)

Gem::Specification.new do |gem|
  gem.authors       = ["Iain Barnett"]
  gem.email         = ["iainspeed@gmail.com"]
  gem.description   = %q{Set up and access for a Sequel model library.}
  gem.summary       = %q{Set up and access for a Sequel model library. Handy.}
  gem.files         = `git ls-files`.split($\)
  gem.executables   = gem.files.grep(%r{^exe/}).map{ |f| File.basename(f) }
  gem.bindir        = "exe"
  gem.test_files    = gem.files.grep(%r{^(test|spec|features)/})
  gem.name          = "repo"
  gem.require_paths = ["lib"]
  gem.version       = Repo::VERSION
  gem.add_dependency( 'sequel', '>=1.3.0')
  gem.add_dependency( 'addressable', '~>2.3')
  gem.add_dependency( "docopt", ">=0.6.0" )
  gem.add_dependency("pathname-glob", ">=0.1.20170628")
  gem.add_dependency("rainbow", ">=2.2.2")
  gem.add_dependency("open4", ">=1.3.4")
  gem.add_dependency("awesome_print", ">=1.8.0")
  gem.add_dependency("tilt", ">= 2.0.8")
  gem.add_dependency("pry", ">= 0.11.3")
end
