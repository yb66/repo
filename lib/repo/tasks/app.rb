namespace :app do

  require 'repo/environment'

  desc "Initialize the environment"
  task :init do
    Sequel.extension :migration
    Repo::Environment.basic_environment(:site_settings)
    Repo::Environment.basic_environment(:secret_settings)

    if Pathname("config/initializers/sequel.yml").exist? && defined?( Sequel )
      Repo::Environment.basic_environment(:sequel) 
      DB = Sequel.connect(Repo::Environment.set_connstring())
    end
  end


  desc "Start the app"
  task :run => :init do
    label = %Q!#{ENV["REVERSE_DOMAIN"].downcase}.#{ENV["PROJECT_NAME"].downcase}!
    plist = "#{label}.plist"
    cmd = "launchctl load ~/Library/LaunchAgents/#{plist}"
    %x{#{cmd}}
    %x{launchctl list | grep #{label}}
  end


  desc "Stop the app"
  task :stop => :init do
    cmd = "launchctl list #{label} > /dev/null 2>&1"
  end

end