namespace :db do
  require "sequel"
  require 'repo/environment'

	namespace :environment do

		desc "Set up the database environment"
		task :setup do
		  Pathname(".").mkpath "config/initializers"
		  Pathname(".").mkpath "db/migrations"
		  Pathname("db/backups/").mkpath
		  # write standard files unless they already exist
		end

    desc "Initialize the environment"
    task :init do
      unless (this = ENV["THIS_IS_HEROKU"]) && this == "true"
        @prefix = defined?(BIN) ? BIN : "bin/"
      else
        @prefix = "bin/"
      end
      if ENV["extra_settings"]
        Repo::Environment.basic_environment Pathname(ENV["extra_settings"])
      end
      Repo::Environment.basic_environment(:site_settings)
      Repo::Environment.basic_environment(:secret_settings)
      Repo::Environment.basic_environment(:sequel)
      DB = Sequel.connect(Repo::Environment.set_connstring())
		end

		task :show do
			@prefix = "bin/"
			Sequel.extension :migration
			ENV["RAKE_DEBUG_ENVIRONMENT"] = "true"
      Repo::Environment.basic_environment(:site_settings)
      Repo::Environment.basic_environment(:secret_settings)
      Repo::Environment.basic_environment(:sequel)
		end
	end

	task :init do
		Rake::Task['db:environment:init'].invoke
	end

  desc "Create a new Postgres database and login role"
  task :create => [:"init", :"create:db", :"create:role" ]

  task :drop => [:init, :"drop:db" ]

  namespace :drop do
    desc "Drop a Postgres database"
    task :db => :init do
      cmd = "dropdb #{ENV['DB_APP']}"
      warn cmd
      system cmd
    end
  end

  namespace :create do
    desc "Create a new Postgres database"
    task :db => :init do
      cmd = "createdb -T template1 #{ENV['DB_APP']}"
      warn cmd
      system cmd
    end

    task :role => :init do
      cmd = "createuser -l #{ENV['DB_APP']}"
      warn cmd
      system cmd
    end
  end



  def get_version_of_database
    DB[:schema_info].first[:version]
  end


  desc "Schema info"
  task :version => :init do
    puts "Version of database is #{get_version_of_database.to_s}"
  end


  desc "Open a local sequel console"
  task :console => :"console:local"
  namespace :console do
    desc "Open a local sequel console"
    task :local => :"environment:init" do
      if ENV["devmode"]
        warn "devmode on"
        ENV["RACK_ENV"] = "development"
      end
      exec %Q!#{@prefix}/sequel #{ENV["DATABASE_URL"]} -r ./db/sequel_convenience.rb!
    end
  end

  task :restore => :"restore:local"
  namespace :restore do
    require "addressable/uri"
    desc "Restore from a downloaded dump\ne.g. rake db:restore version=b022 db=new_midas\nversion is required."
    task :local => :"environment:init" do
      version = ENV["version"]
      dry_run = ENV["dry_run"]
      raise "No VERSION/filename was provided" if version.nil?
      v = version.dup

      if v.start_with? "db/backups"
        v = File.basename v
      end
      db = ENV["DATABASE_URL"]

      uri = Addressable::URI.parse(db)
      s = "pg_restore --verbose --clean --no-acl --no-owner -h #{uri.host} -U #{uri.user} -d #{uri.path.sub("/", "")} db/backups/#{v}"
      dry_run ? puts(s) : exec( s )
    end
  end

  desc "Make a backup"
  task :backup => :init do
    warn ENV["DB_APP"]
    `pg_dump #{ENV["DB_APP"]} > db/backups/#{ENV["DB_APP"]}_#{Time.now.strftime "%Y-%m-%d_%H%I"}.db.bak`
  end


  desc "Perform migration up to latest migration available"
  task :migrate => :"migrate:up"
  namespace :migrate do

    task :init => :"environment:init" do
      Sequel.extension :migration
    end

    desc "Perform migration reset (full erase and migration up)"
    task :trash => :init do
      DB.tables.each{|t| DB.drop_table t unless t == :schema_info }
      DB[:schema_info].update(:version => 0)
      puts "<= sq:migrate:trash executed"
    end


    desc "Perform migration reset (full erase and migration up)"
    task :reset => [:init,:down,:up] do
      DB.tables.each{|t| DB.drop_table t unless t == :schema_info }
      DB[:schema_info].update(:version => 0)
      db_migrate_up
      puts "<= sq:migrate:reset executed"
    end


    @migration_path = "db/migrations"

    desc "Perform migration up/down to VERSION"
    task :to => :init do
      puts "Version of database is #{get_version_of_database.to_s}"
      version = ENV["version"]
      raise "No VERSION was provided" if version.nil?
      Sequel::Migrator.run(DB, @migration_path, :target => version.to_i)
      puts "<= sq:migrate:to version=[#{version}] executed"
      puts "Version of database is #{get_version_of_database.to_s}"
    end

    def db_migrate_up
      Sequel::Migrator.run(DB, @migration_path)
    end

    desc "Perform migration up to latest migration available"
    task :up => :init do
      db_migrate_up
      puts "<= sq:migrate:up executed"
    end

    desc "Perform migration down (erase all data)"
    task :down => :init do
      Sequel::Migrator.run(DB, @migration_path, :target => 0)
      puts "<= sq:migrate:down executed"
    end
  end

end