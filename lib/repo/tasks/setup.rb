require 'erb'
require_relative '../environment.rb'
Repo::Environment.config_dir = Pathname(".").join("config")

desc "Set up files and stuff."
task :setup => :"setup:config"
namespace :setup do
  require 'repo/environment'

	namespace :environment do

		desc "Set up the database environment"
		task :setup do
		  Pathname("config/initializers").mkpath 
		  Pathname("db/migrations").mkpath
		  Pathname("db/backups").mkpath
		end

    desc "Initialize the environment"
    task :init do
      Repo::Environment.basic_environment(:site_settings)
    end
	end

  Template_dir = Pathname(__dir__).join("../templates")

  desc "Set up some config files"
  task :config => :"setup:environment:setup" do
    Rake::Task['setup:site_settings'].invoke
    Rake::Task['setup:secret_settings'].invoke
    Rake::Task['setup:puma'].invoke
    Rake::Task['setup:launchd'].invoke
    Rake::Task['setup:sequel:settings'].invoke
    Rake::Task['setup:sequel:convenience'].invoke
    Rake::Task['setup:sequel:migration'].invoke
  end


  desc "Set up site_settings file in config dir"
  task :site_settings do
    file_name = "site_settings.yml"
    file_path = Repo::Environment.config_dir.join(file_name)
    if !file_path.exist? || ENV["FORCE_REDO"]
      commented = {}
      run_local_port = (rand(1000) + 4000).to_s
      static_local_port = 4567.to_s
      project_dir = Pathname(".").realpath
      project_name  = Pathname(".").realpath.basename
      short_path = %Q!/Users/iainb/.gem/ruby/#{ENV["RUBY_VERSION"]}/bin:/Users/iainb/Library/Frameworks/Ruby.framework/Versions/ruby-#{ENV["RUBY_VERSION"]}/Programs:/usr/local/git/bin:/usr/local/bin!
      gem_path = (ENV["GEM_PATH"].nil? || ENV["GEM_PATH"].empty?) ?
      %Q!/Users/iainb/.gem/ruby/#{ENV["RUBY_VERSION"]}:/Users/iainb/Library/Frameworks/Ruby.framework/Versions/ruby-#{ENV["RUBY_VERSION"]}/lib/ruby/gems/#{ENV["RUBY_VERSION"].sub /\d$/, "0"}! :
        ENV["GEM_PATH"]

      site_url = ENV["SITE_URL"]
      commented["site_url"] = site_url.nil? || site_url.empty?
      site_name = ENV["SITE_NAME"]
      commented["site_name"] = site_name.nil? || site_name.empty?
      site_name_short = ENV["SITE_NAME_SHORT"]
      commented["site_name_short"] = site_name_short.nil? || site_name_short.empty?
      puma_config = ENV["PUMA_CONFIG"] || "config/puma.dev.rb"
      reverse_domain = site_url ? site_url.split(".").reverse.join(".") : "uk.me.iainbarnett"

      staging_site_url = ENV["STAGING_SITE_URL"]
      commented["staging_site_url"] = staging_site_url.nil? || staging_site_url.empty?

      site_description = ENV["SITE_DESCRIPTION"]
      commented["site_description"] = site_description.nil? || site_description.empty?

      tracking_code = ENV["TRACKING_CODE"]
      commented["tracking_code"] = tracking_code.nil? || tracking_code.empty?

      erb = ERB.new Template_dir.join("#{file_name}.erb").read
      file_path.write erb.result(binding)
    end
  end

  desc "Set up launchd plist in launchagents"
  task :launchd => :"setup:environment:init" do
    label = %Q!#{ENV["REVERSE_DOMAIN"].downcase}.#{ENV["PROJECT_NAME"].downcase}!
    file_name = %Q!#{label}.plist!
    file_path = Pathname(ENV["HOME"]).join("Library/LaunchAgents").join(file_name)
    if !file_path.exist? || ENV["FORCE_REDO"]
      commented = {}

      site_name = ENV["SITE_NAME"]
      commented["site_name"] = site_name.nil? || site_name.empty?

      db_app = ENV["DB_APP"]
      commented["db_app"] = db_app.nil? || db_app.empty?

      database_url = ENV["DATABASE_URL"]
      commented["database_url"] = database_url.nil? || database_url.empty?

      erb = ERB.new Template_dir.join("launchagent.plist.erb").read
      file_path.write erb.result(binding)
    end
  end

  desc "Set up puma config file in config dir"
  task :puma => :"setup:environment:init" do
    file_name = "puma.dev.rb"
    file_path = Repo::Environment.config_dir.join(file_name)
    if !file_path.exist? || ENV["FORCE_REDO"]
      erb = ERB.new Template_dir.join("#{file_name}.erb").read
      file_path.write erb.result(binding)
    end
  end

  namespace :sequel do
    desc "Set up convenience/console settings file in db dir"
    task :convenience => :"setup:environment:init" do
      file_name = "convenience.rb"
      file_path = Pathname("db").join(file_name)
      if !file_path.exist? || ENV["FORCE_REDO"]
        erb = ERB.new Template_dir.join("sequel_#{file_name}.erb").read
        file_path.write erb.result(binding)
      end
    end

    desc "Set up sequel settings file in config initializer dir"
    task :settings => :"setup:environment:init" do
      file_name = "sequel.yml"
      file_path = Repo::Environment.config_dir.join("initializers").join(file_name)
      if !file_path.exist? || ENV["FORCE_REDO"]
        commented = {}

        db_app = ENV["DB_APP"] || ENV["PROJECT_NAME"].downcase
        commented["db_app"] = db_app.nil? || db_app.empty?

        database_url = ENV["DATABASE_URL"]
        commented["database_url"] = database_url.nil? || database_url.empty?

        erb = ERB.new Template_dir.join("#{file_name}.erb").read
        file_path.write erb.result(binding)
      end
    end


    desc "Set up a first migration file in db migrations dir"
    task :migration => :"setup:environment:init" do
      file_name = "001_project_name.rb"
      file_path = Pathname("db/migrations").join(file_name.sub "project_name", ENV["PROJECT_NAME"])
      if !file_path.exist? || ENV["FORCE_REDO"]
        erb = ERB.new Template_dir.join("#{file_name}.erb").read
        file_path.write erb.result(binding)
      end
    end
  end


  desc "Set up secret_settings file in config dir"
  task :secret_settings => :"setup:environment:init" do
    file_name = "secret_settings.yml"
    file_path = Repo::Environment.config_dir.join(file_name)
    if !file_path.exist? || ENV["FORCE_REDO"]
      commented = {}

      redistogo_url_port = ENV["REDISTOGO_URL_PORT"] || "6379"
      commented["redistogo_url_port"] = redistogo_url_port.nil? || redistogo_url_port.empty?

      database_user = ENV["DATABASE_USER"]
      commented["database_user"] = database_user.nil? || database_user.empty?

      database_name = ENV["DATABASE_NAME"]
      commented["database_name"] = database_name.nil? || database_name.empty?

      github_token = ENV["github_token"]
      commented["github_token"] = github_token.nil? || github_token.empty?

      bitbucket_token = ENV["BITBUCKET_TOKEN"]
      commented["bitbucket_token"] = bitbucket_token.nil? || bitbucket_token.empty?

      heroku_user = ENV["HEROKU_USER"]
      commented["heroku_user"] = heroku_user.nil? || heroku_user.empty?

      amazon_access_key = ENV["AMAZON_ACCESS_KEY"]
      commented["amazon_access_key"] = amazon_access_key.nil? || amazon_access_key.empty?

      iam_user = ENV["IAM_USER"]
      commented["iam_user"] = iam_user.nil? || iam_user.empty?

      amazon_access_key = ENV["AMAZON_ACCESS_KEY"]
      commented["amazon_access_key"] = amazon_access_key.nil? || amazon_access_key.empty?

      amazon_access_key_id = ENV["AMAZON_ACCESS_KEY_ID"]
      commented["amazon_access_key_id"] = amazon_access_key_id.nil? || amazon_access_key_id.empty?

      amazon_secret_access_key = ENV["AMAZON_SECRET_ACCESS_KEY"]
      commented["amazon_secret_access_key"] = amazon_secret_access_key.nil? || amazon_secret_access_key.empty?

      erb = ERB.new Template_dir.join("#{file_name}.erb").read
      file_path.write erb.result(binding)
    end
  end
end