require 'docopt'
require 'pathname'
require_relative "./environment.rb"
require_relative "./logging.rb"

if ENV["DEBUG"]
  require 'pry-byebug'
  require 'pry-state'
  binding.pry
end

module Repo
  module Cmd
    class Base
      class << self
        def reset
          @exe = nil
          @env = nil
        end

        def run( *opts, **options )
          @exe ||= new
          @exe.run *opts, options
        end


        def usage
          self.const_get "USAGE"
        end


        def env
          @env ||= ::Repo::Environment.new
        end

        def env= env
          @env = env
        end
      end


      def run( *opts, **options )
        if opts and not opts.empty?
          @options = opts.first
        else
          @options = Docopt::docopt(self.class.usage, options)
        end
        execute
      rescue Docopt::Exit => e
        warn e.message
      end


      attr_reader :options


      def logger
        if @logger.nil?
          setup_logger
          env.logger = @logger if env.logger.nil?
        end
        @logger
      end


      def setup_logger
        @logger = ::Repo::Logging.instance
        if options["--quiet"]
          ::Repo::Logging.level = :quiet
        elsif options["--verbose"]
          if options["--verbose"] == 1
            ::Repo::Logging.level = :verbose
            logger.verbose "Set to verbose", colour: :magenta
          elsif options["--vverbose"] or (options["--verbose"] and options["--verbose"] == 2)
            ::Repo::Logging.level = :vverbose
            logger.verbose "Set to very verbose", colour: :magenta
          elsif options["--debug"] or (options["--verbose"] and options["--verbose"] == 3)
            ::Repo::Logging.level = :debug
            logger.verbose "Set to debug", colour: :magenta
          else # for now but need a middle
            ::Repo::Logging.level = :verbose
            logger.verbose "Set to verbose", colour: :magenta
          end
        end
        @logger
      end

      def env
        @env ||= self.class.env
      end
    end
  end
end