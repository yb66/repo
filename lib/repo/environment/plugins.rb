module Repo
  module Plugins
    def sequel env
      env.each do |k,v|
        case k
        when "extensions", "plugins"
          key = "sequel_#{k}".upcase
          val = v.map(&:to_s).join(",")
        else
          key = k.upcase
          val = v.to_s
        end
        @logger.debug? ?
          @logger.debug("  #{key}: #{val}") :
          @logger.vverbose("  #{key}")
        
        ENV[key] = val
      end
    end
  end


  class Environment
    include Plugins
  end
end