require "singleton"
require 'rainbow'
require 'awesome_print'

module Repo
  class Logging
    include Singleton
    VERBOSITY = {
      :quiet    => {rank: 0, colour: nil },
      :verbose  => {rank: 1, colour: :green},
      :vverbose => {rank: 2, colour: :cyan},
      :debug    => {rank: 3, colour: :yellow},
    }

    class << self
      def level=( level )
        @level = level      
      end
      def level
        @level ||= :quiet
      end

      def logger opts={}
        if @logger.nil?
          @logger = ::Repo::Logging.instance
        end
        if @level.nil?
          if lvl = %w{quiet verbose vverbose debug}.find{|lvl| opts.any?{|k,_| lvl == k } }
            self.level = lvl.to_sym
          else
            self.level = :verbose
          end
        end
        @logger
      end
    end


    def at_least? level
      VERBOSITY[self.class.level][:rank] >= VERBOSITY[level][:rank]
    end

    %w{verbose vverbose debug}.zip(%w{green cyan yellow}).each do |lvl,clr|
      define_method lvl do |stuff, colour:nil,&block|        
        return unless at_least? lvl.to_sym
        colour ||= clr
        warn Rainbow(stuff).send colour
        if block
          retval = block.call
          if retval.respond_to? :each
            ap retval
          else
            warn Rainbow(retval).silver 
          end
          retval
        end
      end
    end


    %w{quiet verbose vverbose debug}.each do |lvl|
      define_method "#{lvl}?" do
        self.class.level == lvl.to_sym
      end
    end


    def output_unless_quiet stuff
      warn stuff unless quiet?
    end


  end
end