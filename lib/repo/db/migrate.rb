module Repo

  # migrations
  module Migrate

    DEFAULT_MIGRATION_PATH = Pathname("db/migrations")


    def migrate_init
      if @path.nil?
        @path = DEFAULT_MIGRATION_PATH
      end
      Sequel.extension :migration
    end


    # "Perform migration reset (full erase and migration up)"
    def trash
      migrate_init
      db.tables.each{|t| db.drop_table t unless t == :schema_info }
      db[:schema_info].update(:version => 0)
      warn "<= sq:migrate:trash executed"
    end


    # "Perform migration reset (full erase and migration up)"
    def reset
      migrate_init
      down
      db.tables.each{|t| db.drop_table t unless t == :schema_info }
      db[:schema_info].update(:version => 0)
      up
      warn "<= sq:migrate:reset executed"
    end

    # "Perform migration up/down to VERSION"
    def to version
      migrate_init
      @logger.verbose "Version of database is #{self.version}"
      raise "No VERSION was provided" if version.nil?
      Sequel::Migrator.run(db, @path, :target => version.to_i)
      @logger.verbose "<= sq:migrate:to version=[#{version}] executed"
      @logger.verbose "Version of database is now #{self.version}"
    end


    # "Perform migration up to latest migration available"
    def up
      migrate_init
      Sequel::Migrator.run(db, @path)
      @logger.verbose "<= sq:migrate:up executed"
    end

    # "Perform migrationwn (erase all data)"
    def down
      migrate_init
      Sequel::Migrator.run(db, @path, :target => 0)
      @logger.verbose "<= sq:migrate:down executed"
    end
  end
end