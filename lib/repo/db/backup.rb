require 'pathname'

module Repo
  module Backup

    DEFAULT_BACKUP_PATH = Pathname("db/backup")

    def backup_init
      init # Just to be sure
      if @path.nil?
        @path = DEFAULT_BACKUP_PATH
      end
    end

    # "Restore from awnloaded dump\ne.g. rake db:restore version=b022 db=new_midas\nversion is required."
    def restore filepath=nil, custom: false
      @logger.output_unless_quiet "Before restoring an SQL dump, all the users who own objects or were granted permissions on objects in the dumped database must already exist."
      backup_init

      if filepath.nil?
        pn = @path
      else
        pn = Pathname(filepath)
      end
      if pn.exist?
        if pn.file?
          file = pn
        # elsif's not stricly necessary but I want it clear.
        elsif pn.directory? # find latest file with .bak        
          file = 
            pn.children
              .reject{|n| n.directory? or n.to_s.start_with?(".") }
              .sort_by{|f| test(?M, f) }
              .min
        end
      else
        if pn != DEFAULT_BACKUP_PATH and DEFAULT_BACKUP_PATH.join(pn).exist?
          file = DEFAULT_BACKUP_PATH.join(pn)
        else
          warn "Can't find the file to restore, please provide a full path."
        end
      end
      @logger.vverbose "Restoring from #{file.to_s}"

      uri = Addressable::URI.parse(self.connstring)
      if custom
        cmd = "pg_restore --verbose --clean --no-acl --no-owner -h #{uri.host} -U #{uri.user} -d #{uri.path.sub("/", "")} #{file}"
      else
        cmd = "psql #{uri.path.sub("/", "")} < #{file}"
      end
      @logger.debug cmd
      exec( cmd ) unless env.dry_run?
    end


    # "Make a backup"
    def backup filepath=nil, custom: false
      backup_init
      time = Time.now.strftime "%Y-%m-%d_%H%I"
      db_app = ENV["DB_APP"]
      filename = "#{db_app}_#{time}.db.bak"

      if filepath.nil?
        file = @path.join filename
      else
        pn = Pathname(filepath)        
        if pn.exist?
          if pn.directory?
            file = pn.join filename
          else
            @logger.output_unless_quiet "That file exists. This command won't overwrite it so (re)move it and then re-run."
            abort
            #file = pn
          end
        else
          if pn.to_s.end_with? "/"
            file = pn.join filename
          elsif pn.to_s =~ %r{/}
            file = pn
          else
            file = @path.join filename
          end
        end
      end

      if custom
        cmd ="pg_dump -Fc #{db_app} > #{file}"
      else
        cmd = %Q!pg_dump #{db_app} > #{file}!
      end
      @logger.vverbose cmd
      exec cmd unless env.dry_run?
    end

    alias_method :dump, :backup


    def get_file_at path
      fail if path.nil? or path.to_s.empty?
      pn = Pathname(path)
    end
  end
end