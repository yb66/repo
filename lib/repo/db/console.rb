require 'pry'

module Repo

  module Console
    # "Open a local sequel console"
    def console_local devmode: false, models: nil
      if devmode
        env.logger.output_unless_quiet "devmode on"
        ENV["RACK_ENV"] = "development"
        env.load_all
      else
        env.load :sequel
      end
      env.set_connstring

      require 'logger'
      require_relative "../../repo.rb"

      logger = Logger.new STDOUT 
      logger.level = Logger::WARN
      logger.datetime_format = '%a %d-%m-%Y %H%M '
      Repo::Config.logger = logger

      if models.nil?
        pn = Pathname("app/models")
        if pn.exist?
          models = pn
        else
         pn = Pathname("lib/models")
         models = pn if pn.exist?
        end
      end
      #::DB.loggers << logger
      warn "ENV['DATABASE_URL'] = #{ENV['DATABASE_URL']}"
      Repo::Config.connstring = ENV['DATABASE_URL']
      Repo::Config.model_dirs << models unless models.nil?
      Repo.db# = ::DB
      unless defined? DB
        Object.const_set("DB", Repo.db)
      end
      Repo.require_models
      sq_conv = Pathname("./db/sequel_convenience.rb")
      load sq_conv if sq_conv.exist?
      Pry.start
    end

  end
end