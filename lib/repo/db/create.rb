require 'open4'
require 'tilt/erb'
require_relative "../../repo.rb"

module Repo
  module Create

    # "Create a new Postgres database"
    def create_db
      @logger.vverbose "Creating database…"
      cmd1 = %Q!createdb -O #{role} -T template1 #{name}!
      cmd2 = %Q!echo 'revoke connect on database "#{name}" from public;' | psql -qAt!
      if db_exists?
        @logger.vverbose "Database exists."
      else
        @logger.vverbose cmd1
        @logger.vverbose cmd2
        unless env.dry_run?
          pid, stdin, stdout, stderr = Open4::popen4(cmd1)
          _, status = Process::waitpid2 pid
          if status.success?
            system cmd2
          end
        end
      end
    end


    def role_exists? role=nil
      role ||= self.role || ENV["DATABASE_ROLE"] || ENV['DB_APP'] || "vagrant"
      @logger.vverbose "Role '#{role}' exists?"
      cmd = %Q!psql postgres -tAc " SELECT 1 FROM pg_roles WHERE rolname='#{role}'"!
      output = `#{cmd}`
      !output.strip.empty?
    end


    def create_role
      @logger.vverbose "Creating role…"
      role = self.role || ENV["DATABASE_ROLE"] || ENV['DB_APP'] || "vagrant"
      if role_exists? role
        @logger.vverbose "Role exists"
      else
        cmd = %Q!createuser -l "#{role}"!
        @logger.vverbose cmd
        system cmd unless env.dry_run?
      end
    end


    def grant_role
      @logger.vverbose "Granting privileges to role…"

      if role.nil?
        @logger.vverbose "Role not given. Using env…"
      end
      @logger.vverbose "Role set to #{role}"

      if name.nil?
        @logger.vverbose "Database name not given. Using env…"
      end
      @logger.vverbose "Database set to #{name}"


      template = Tilt::ERBTemplate.new(TEMPLATES.join("sql_grant.erb"))
      cmd = "echo '#{template.render(Object.new, name: name, role: role).chomp.strip}' | psql -d #{name}"
      @logger.vverbose "Command to run is:\n\n#{cmd}" do
        system cmd unless env.dry_run?
      end
    end

  end
end