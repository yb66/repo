require 'yaml'
require 'pathname'
require 'pathname-glob'
require 'singleton'
require_relative "./logging.rb"

module Repo

  # Handles setting up the environment.
  class Environment

    attr_accessor :dry_run, :config_files, :role, :name
    attr_writer :logger

    # This is where configs will normally be found.
    DEFAULT_CONFIG_LOCATION = "config"


    def initialize logger: nil, configs: nil
      @config_files = {}
      @loaded_configs = {}
      @logger = logger
      if configs
        process configs
      end
    end


    def logger
      if @logger.nil?
        @logger = Logging.logger
      end
      @logger
    end


    # Is a prefix needed for calling executables? Pass the name
    # of the exe and this will either prefix it or not depending
    # on whether it's true.
    def prefix exe
      if defined? Bundler
        if Bundler.bin_path.empty? or Bundler.bin_path.nil?
          exe
        else
          pn_exe = Pathname(Bundler.bin_path).join(exe)
          if pn_exe.exist?
            pn_exe.to_s
          else
            exe
          end
        end
      end
    end


    # Is the dry run option set?
    def dry_run?
      @dry_run
    end


    # Set a location to look for configs.
    def configs=( location )
      process location
    end


    # Run this to finish off setup and make sure that configs
    # have been set.
    def complete_setup!
      if @config_files.empty?
        process DEFAULT_CONFIG_LOCATION
      end
      true
    end


    # Helper for inding and storing paths to configs.
    def process location
      if location
        if location.respond_to? :each # Array
          location.each do |dir|
            Pathname(dir).glob("**/*.yml") do |pn|
              @config_files[pn] = nil
            end
          end
        else
          Pathname(location).glob("**/*.yml") do |pn|
            @config_files[pn] = nil
          end
        end
      else
        Pathname("config").glob("**/*.yml") do |pn|
          @config_files[pn] = nil
        end
      end
      @unloaded_configs = @config_files.dup
      location
    end


    # Mungs the connstring together and sets it to the
    # DATABASE_URL env var.
    def set_connstring url=nil, refresh: false, role: nil, name: nil, test: false
      if @url.nil? or refresh
        if url
          @url = url
        elsif ENV['DATABASE_URL']
          @url = ENV['DATABASE_URL']
        else
          if role
            self.role = role if self.role.nil?
          elsif self.role 
            role = self.role
          else
            self.role = role = ENV["DB_ROLE"] || ENV["DB_APP"]
          end


          if ENV["DB_APP"].nil?
            load :sequel
          end

          if name and self.name.nil?
            self.name = name
          elsif self.name and name.nil?
            name = self.name
          elsif name.nil? and self.name.nil?
            name = ENV["DB_APP"]
            self.name = name
          end
          if test or ENV["RACK_ENV"] == "test"
            name = "#{name}.test"
            self.name = name
          end
          @url = %Q!postgres://#{role}@localhost/#{name}!
        end
        ENV['DATABASE_URL'] = @url
      end
      logger.debug? ?
        logger.debug("  DATABASE_URL: #{@url}") :
        logger.vverbose("  DATABASE_URL")

      @url
    end


    # JUST GIVE ME ALL OF IT!
    def load_all refresh: false
      logger.vverbose "Loading all configs"
      configs = refresh ?
        @config_files :
        @unloaded_configs

      logger.debug( "configs:" ) { configs.keys }
      configs.each do |path,_|
        self.load path, refresh: refresh
      end
    end


    # Load a specific config by name or any config that has the name
    # as part of its name.
    def load( name, loader: "yaml_file", refresh: false )
      config_files = refresh ?
        @config_files :
        @unloaded_configs

      configs = self.select name, config_files: config_files

      configs.each do |conf|
        if conf.exist?
          yaml_file conf.to_path
          @unloaded_configs.delete conf
        else
          logger.vverbose "Couldn't find that settings file: #{conf.to_path}"
        end
      end
    end


    # Helper for finding the right configs to load.
    def select name, config_files: nil
      configs = []
      config_files ||= @config_files
      if name.respond_to?(:to_path) and config_files.has_key?(name)
        configs << name
      else
        name = name.to_s
        config_files.each do |path,_|
          if path.basename.to_s == name or path.basename(".*").to_s == name
            configs << path
          end
        end
      end
      configs
    end


    # Helper for making an encryption key for cookies.
    # Loads a random string into LLAVE.
    def load_secret_key_for_encryption_of_cookies refresh: false
      if refresh or !ENV["LLAVE"]  # secret key for encryption of cookies
        require 'securerandom'
        ENV["LLAVE"] = SecureRandom.urlsafe_base64(32)
      end
    end


    def launchd( file )
      env = YAML.load_file(file)
      env && env.each do |k,v|
        logger.vverbose "<key>#{k.upcase}</key>"
        logger.vverbose "<string>#{v}</string>"
      end
    end


    # Load a YAML file into the environment.
    def yaml_file file
      logger.verbose "Setting up environment from #{file}..."
      hs = YAML.load_file(file)
      if hs
      # This is here to run plugins that may want to do more with the hash.
        if respond_to?(meth = File.basename(file, File.extname(file)) )
          self.send meth, hs
        else
          hs.each do |k,v|
            logger.debug? ?
              logger.debug("  #{k.upcase}: #{v}") :
              logger.vverbose("  #{k.upcase}")
            ENV[k.upcase] = v.to_s
          end
        end
      end
    end

  end
end