require "sequel"
require_relative "environment.rb"
require_relative "logging.rb"
require "addressable/uri"
require_relative "db/migrate.rb"
require_relative "db/console.rb"
require_relative "db/backup.rb"
require_relative "db/create.rb"

module Repo
  class Database
    include Console, Migrate, Create, Backup

    # "Initialize the environment"
    def initialize env, logger:nil, configs: nil, path: nil, role: nil, name: nil, test: false
      @env = env || Repo::Environment.new( configs: "config" )
      @env.load :sequel
      @path = path ? Pathname(path) : nil
      @logger = logger
      # TODO consider a better way to work with vagrant
      @role = role || ENV["DATABASE_ROLE"] || ENV["DB_ROLE"] || ENV['DB_APP'] || "vagrant"
      @name = name || ENV['DB_APP'] || @role
      @test = test
      # Now set up the environment and connect to the database
      init
    end


    attr_reader :env, :role, :name
    attr_accessor :path


    def logger
      if @logger.nil?
        @logger = Logging.logger
      end
      @logger
    end


    def db
      if @db.nil?
        init
        @db = Sequel.connect(connstring)
        define_DB
      end
      @db
    end


    # Sequel uses the DB constant by convention
    def define_DB
      unless defined? ::DB
        Object.const_set(:DB, self.db)
      end
    end


    def connstring conn=nil, refresh: false
      if @connstring.nil? or refresh
        if conn
          @connstring = conn
        else
          init
          opts = {role: @role, name: @name, test: @test}.reject{|_,v| v.nil? }
          @connstring = env.set_connstring opts
        end
      end
      @connstring
    end


    # "Set up the database environment"
    def setup
      %w{config/initializers db/migrations db/backup}.each do |path|
        # write standard files unless they already exist
        logger.verbose "Making path #{path}"
        Pathname(path).mkpath unless env.dry_run?
      end
    end


    def load_configs
      if ENV["extra_settings"]
        env.load Pathname(ENV["extra_settings"])
      end
      env.load(:site_settings)
      env.load(:secret_settings)
      env.load(:sequel)
    end


    def show
      Sequel.extension :migration
      ENV["RAKE_DEBUG_ENVIRONMENT"] = "true"
      env.load(:site_settings)
      env.load(:secret_settings)
      env.load(:sequel)
    end

    def init refresh=false
      if @init.nil? or refresh
        env.load(:sequel)
        @init = true
      end
      @init
    end

    # "Create a new Postgres database and login role"
    #def create => [:"init", :"create:db", :"create:role" ]

    # "Drop a Postgres database"
    def drop
      cmd = "dropdb #{ENV['DB_APP']}"
      warn cmd
      system cmd unless env.dry_run?
    end


    def db_exists?
      logger.vverbose "Database '#{name}' exists?"
      cmd = %Q!psql -lAtq '#{name}'!
      output = `#{cmd}`
      !(output =~ /FATAL/ or output.strip.empty? or not $?.success?)
    end



    # "Schema info"
    def version
      if env.dry_run?
        warn "self.db[:schema_info].first[:version] would be run"
      else
        self.db[:schema_info].first[:version]
      end
    end

  end

end
