# encoding: UTF-8
require 'pathname'
require 'logger' # logging
require 'sequel' # the Sequel library

# Sequel repository set up and access.
module Repo

  TEMPLATES = Pathname(__dir__).join("repo/templates")

  # Config settings for the repo.
  module Config

    # @param [String] conn
    def self.connstring=( conn )
      @db_config = conn
    end

    # @return [String]
    def self.connstring
      @db_config
    end


    # @param [Logger] logger_instance
    def self.logger=( logger_instance )
      fail ArgumentError, "That's not a logger instance!" unless logger_instance.respond_to? :log
      @logger = logger_instance
    end


    # @return [Logger,nil]
    def self.logger
      @logger ||= nil
    end


    # @param [Array<Symbol>] exts An array of the symbol names of the extensions to load.
    def self.extensions=( exts )
      @extensions = exts
    end

    # @return [Array<Symbol>]
    def self.extensions
      if @extensions.nil?
        @extensions =
          if ENV["SEQUEL_EXTENSIONS"]
            ENV["SEQUEL_EXTENSIONS"].split(",").map(&:to_sym)
          else
            []
          end
      end
      @extensions
    end

    class ModelDirs < Array
    
#       def exception(message="Pass a valid directory object or string.")
#         ArgumentError.new message
#       end
    
      # @param [String,Dir] dir
      def push( dir )
        fail self if (dir.respond_to?(:empty?) && dir.empty?) || dir.nil?
        # For some reason `require` is using __dir__ as the
        # working dir at times, so adding this in.
        dir = Pathname dir
        dir = Pathname.pwd.join( dir ) unless dir.absolute?
        super( dir )
      rescue Error
        raise
      rescue => error
        raise Error.new( "#{error.class}: #{error.message}", error) 
      end
      
      # @see #push
      alias :<< push
    
      # Error handling
      class Error < StandardError
        attr_reader :original
        def initialize(msg, original=$!)
          super(msg)
          @original = original
        end
      end
    end

    # @return [Array<String>]
    def self.model_dirs
      @model_dirs ||= ModelDirs.new
    end
    
  end # Config
    
  def self.extensions_already_loaded?
    !!@extensions_already_loaded
  end

  def self.models_required_already?
    !!@models_required_already
  end

  # @param [Array<Symbol>] exts An array of the symbol names of the extensions to load.
  def self.load_extensions( extensions=nil )
    extensions ||= Repo::Config.extensions
    Sequel.extension *extensions
    @extensions_already_loaded = true
  end


  def self.db
    if @db.nil?
      @db = Sequel.connect( Repo::Config.connstring )
      Repo.load_extensions unless extensions_already_loaded?
      Repo.require_models unless models_required_already?

      if Repo::Config.logger
        @db.loggers << Repo::Config.logger unless @db.loggers.include? Repo::Config.logger
      end
    end

    @db
  end


  def connected?
    !@db.nil?
  end


  # @param [Sequel::Database]
  def self.db=( db )
    @db = db
  end


  # @param [Array<String>] model_dirs
  def self.require_models(model_dirs=nil, refresh: false)
    return if @models_required_already and refresh == false
    if model_dirs.nil?
      model_dirs = Config.model_dirs
    else
      model_dirs = Pathname(model_dirs)
    end
    model_dirs.each do |dir|
      next unless dir.directory?
      dir.each_child do |file|
        next unless file.file?
        next if file.basename.to_s =~ /^\./
        require file
      end
    end
    @models_required_already = true
  end
  
  
end # Repo
